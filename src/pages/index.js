import Head from 'next/head';
import { Inter } from 'next/font/google';
import { BsFillMoonStarsFill } from 'react-icons/bs';
import {
  AiFillTwitterCircle,
  AiFillLinkedin,
  AiFillYoutube,
} from 'react-icons/ai';
import Image from 'next/image';
import deved from '../../public/dev-ed-wave.png';
import design from '../../public/design.png';
import code from '../../public/code.png';
import consulting from '../../public/consulting.png';
import web1 from '../../public/web1.png';
import web2 from '../../public/web2.png';
import web3 from '../../public/web3.png';
import web4 from '../../public/web4.png';
import web5 from '../../public/web5.png';
import web6 from '../../public/web6.png';
import { useState } from 'react';

const inter = Inter({ subsets: ['latin'] });

export default function Home() {
  const [darkMode, setDarkmode] = useState(false);
  return (
    <div className={darkMode ? 'dark' : ''}>
      <Head>
        <title>Iyyappan Portfolio</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="bg-white px-10 md:px-20 lg:px-40 dark:bg-black dark:text-white">
        <section className="min-h-screen">
          <nav className="py-10 mb-12 flex justify-between">
            <h1 className="text-xl font-burtons">Iyyappan</h1>
            <ul className="flex items-center">
              <li>
                <BsFillMoonStarsFill
                  onClick={() => setDarkmode(!darkMode)}
                  className="cursor-pointer text-2xl"
                />
              </li>
              <li>
                <a className="bg-cyan-500 px-4 py-2 rounded-md ml-8" href="#">
                  Resume
                </a>
              </li>
            </ul>
          </nav>
          <div className="text-center p-10">
            <h2 className="text-5xl py-2 text-teal-600 font-medium">
              Iyyappan Murugan
            </h2>
            <h3 className="text-2xl py-2">Software Engineer</h3>
            <p className="text-md py-5 leading-8 text-gray-800 md:text-2xl lg:text-3xl">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
              natus deleniti esse sit non tenetur, earum explicabo enim totam
              iste ex delectus fuga voluptates in voluptate aut distinctio
              aliquid asperiores?
            </p>
          </div>
          <div className="text-5xl flex justify-center gap-16 text-gray-600">
            <AiFillLinkedin />
            <AiFillTwitterCircle />
            <AiFillYoutube />
          </div>
          <div className="w-60 h-60 rounded-full bg-blue-300 mx-auto relative mt-20 pt-8 md:w-96 md:h-96">
            <Image src={deved} />
          </div>
        </section>
        <section>
          <div>
            <h3 className="text-3xl py-1">Services I offer</h3>
            <p className="text-md py-2 leading-8 text-gray-80">
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit
              ipsam aut <span className="text-teal-500">pariatur</span>{' '}
              quibusdam? Minus dolore est laboriosam illo, error, dolorum,
              quibusdam exercitationem cupiditate neque porro itaque{' '}
              <span className="text-teal-500">pariatur</span> odio sunt ducimus!
            </p>
            <p className="text-md py-2 leading-8 text-gray-80">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat
              pariatur, possimus optio fuga repudiandae, amet repellendus dicta
              nihil laborum aperiam debitis dolorem voluptatem id consectetur.
              Dolorem quos ea aperiam ducimus.
            </p>
          </div>
          <div className="lg:flex gap-10">
            <div className="text-center shadow-lg p-10 rounded-xl my-10">
              <Image className="m-auto" src={design}></Image>
              <h3 className="text-lg font-md">Beautiful Designs</h3>
              <p className="py-2">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore
                unde sunt ab culpa nobis ullam alias. Esse, provident velit nisi
                exercitationem dignissimos laborum pariatur nemo deserunt nobis
                quos iusto assumenda.
              </p>
              <h4 className="py-4 text-teal-600">The tools i use</h4>
              <p className="text-gray-800 py-1">Photoshop</p>
              <p className="text-gray-800 py-1">Figma</p>
              <p className="text-gray-800 py-1">Inshot</p>
              <p className="text-gray-800 py-1">XYZ</p>
            </div>

            <div className="text-center shadow-lg p-10 rounded-xl my-10">
              <Image className="m-auto" src={code}></Image>
              <h3 className="text-lg font-md">Coding languages</h3>
              <p className="py-2">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore
                unde sunt ab culpa nobis ullam alias. Esse, provident velit nisi
                exercitationem dignissimos laborum pariatur nemo deserunt nobis
                quos iusto assumenda.
              </p>
              <h4 className="py-4 text-teal-600">The tools i use</h4>
              <p className="text-gray-800 py-1 dark:text-yellow-200 ">React</p>
              <p className="text-gray-800 py-1 dark:text-yellow-200">Redux</p>
              <p className="text-gray-800 py-1 dark:text-yellow-200">
                Javascript
              </p>
              <p className="text-gray-800 py-1 dark:text-yellow-200">XYZ</p>
            </div>

            <div className="text-center shadow-lg p-10 rounded-xl my-10">
              <Image className="m-auto" src={consulting}></Image>
              <h3 className="text-lg font-md">Consulting</h3>
              <p className="py-2">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore
                unde sunt ab culpa nobis ullam alias. Esse, provident velit nisi
                exercitationem dignissimos laborum pariatur nemo deserunt nobis
                quos iusto assumenda.
              </p>
              <h4 className="py-4 text-teal-600">The tools i use</h4>
              <p className="text-gray-800 py-1">Photoshop</p>
              <p className="text-gray-800 py-1">Figma</p>
              <p className="text-gray-800 py-1">Inshot</p>
              <p className="text-gray-800 py-1">XYZ</p>
            </div>
          </div>
        </section>
        <section>
          <h3 className="text-3xl py-1">Portfolio</h3>
          <p className="text-md py-2 leading-8 text-gray-80">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Odit ipsam
            aut <span className="text-teal-500">pariatur</span> quibusdam? Minus
            dolore est laboriosam illo, error, dolorum, quibusdam exercitationem
            cupiditate neque porro itaque{' '}
            <span className="text-teal-500">pariatur</span> odio sunt ducimus!
          </p>
          <div className="flex flex-col gap-10 lg:flex-wrap lg:flex-row">
            <div className="basis-1/3 flex-1">
              <Image src={web1} />
            </div>
            <div className="basis-1/3 flex-1">
              <Image src={web2} />
            </div>
            <div className="basis-1/3 flex-1">
              <Image src={web3} />
            </div>
            <div className="basis-1/3 flex-1">
              <Image src={web4} />
            </div>
            <div className="basis-1/3 flex-1">
              <Image src={web5} />
            </div>
            <div className="basis-1/3 flex-1">
              <Image src={web6} />
            </div>
          </div>
        </section>
      </main>
    </div>
  );
}
